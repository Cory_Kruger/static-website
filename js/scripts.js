/*
	JavaScript scripts
*/
function printCopyright() {
	var date = new Date();
	var year = date.getFullYear();
	document.write("&copy; " + year + " Cory Kruger");
}

function printLastUpdated() {
	document.write("Site last updated:  2017-03-10");
}

function printLogo() {
	document.write("<img src=\"./images/logo.png\" alt=\"Logo image\" />");
}

function printNav() {
	document.write("<br /><h2 class=\"navigation\">NAVIGATION</h2><p class=\"navigation\"><a class=\"left\" href=\"./home.html\">Home</a><br /><a class=\"left\" href=\"./contact.html\">Contact</a><br /><a class=\"left\" href=\"./resume.html\">R&#233;sum&#233;</a></p>");
}

function printSocialMedia() {
	document.write("<!-- LinkedIn, the LinkedIn logo, the IN logo and InMail are registered trademarks or trademarks of LinkedIn Corporation and its affiliates in the United States and/or other countries. -->");
	document.write("<a href=\"http://www.twitter.com/kruger_cory\"><img src=\"./images/twitter.png\" alt=\"Twitter\" width=\"20%\" /></a><br /><br /><a href=\"http://ca.linkedin.com/pub/cory-kruger/37/892/8a2/\"><img src=\"./images/linkedIn.png\" alt=\"LinkedIn\" width=\"20%\" /></a>");
}

function printAge() {
	var current = new Date();
	var bday = new Date("1993-02-27");
	var diff = current - bday;
	var years = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
	document.write(years);
}
